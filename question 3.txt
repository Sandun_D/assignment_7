---Question 2---

#include<stdio.h>
#include<string.h>
int main()
{
    int check=0, r1=0 ,c1=0, r2=0, c2=0;
    printf("Enter the number of rows & columns in matrix 1:");
    scanf("%d%d",&r1,&c1);

    printf("Enter the number of rows & columns in matrix 2:");
    scanf("%d%d",&r2,&c2);
    
    int matrix1[r1][c1],matrix2[r2][c2];
    printf("Enter values of Matrix 1:\n");
    for(int i=0; i<r1; i++)
    {
        for(int j=0; j<c1; j++)
        {
            printf("Enter the data of row %d and column %d :",i+1,j+1);
            scanf("%d",&matrix1[i][j]);
        }
    }
    printf("Matrix 1:\n"); //Printing the matrix 1 using values entered by user
    for(int i=0; i<r1; i++)
    {
        printf("|");
        for(int j=0; j<c1; j++)
        {
            printf("%4d",matrix1[i][j]);
        }
        printf("%4c|\n",'\0');
    }
    printf("\nEnter values of Matrix 2:\n");
    for(int i=0; i< r2; i++)
    {
        for(int j=0; j<c2; j++)
        {
            printf("Enter the data of row %d and column %d :",i+1,j+1);
            scanf("%d",&matrix2[i][j]);
        }
    }
    printf("Matrix 2:\n");
    for(int i=0; i < r2; i++)
    {
        printf("|");
        for(int j=0; j < c2; j++)
        {
            printf("%4d",matrix2[i][j]);
        }
        printf("%4c|\n",'\0');
    }

    if(r1 == r2 && c1== c2)
    {
        int output[r1][c1];
        for(int i=0; i<r1; i++)
        {
            for(int j=0; j < c1; j++)
            {
                output[i][j]=matrix1[i][j]+matrix2[i][j];
            }
        }
        printf("\nMatrix 1 + Matrix 2  :\n");
        for(int i=0; i < r2; i++)
        {
            printf("|");
            for(int j=0; j < c2; j++)
            {
                printf("%4d",output[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Mmatrix 1 and Matrix 2 cannot be added\n");
    }
    if(c1 == r2)
    {
        int output2[r1][c2];
        for(int i=0; i < r1; i++)
        {
            for(int j=0; j < c2; j++)
            {
                output2[i][j]=0;
            }
        }
        for (int i = 0; i < r1; i++)
        {
            for (int j = 0; j < c2; j++)
            {
                for (int k = 0; k < c1; k++)
                {
                    output2[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        printf("\nMatrix 1 * Matrix 2  :\n");
        for(int i=0; i < r1; i++)
        {
            printf("|");
            for(int j=0; j< c2; j++)
            {
                printf("%4d",output2[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Matrix 1 and Matrix 2 cannot be multiplied");
    }
    return 0;
}